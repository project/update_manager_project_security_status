<?php

namespace Drupal\Tests\update_manager_project_security_status\Functional;

use Drupal\Tests\update\Functional\UpdateTestBase;

/**
 * Tests that security coverage info is added to Update Manager report.
 *
 * Test is modeled after core Update Manager test, which uses mock XML data.
 *
 * @group update_manager_project_security_status
 */
class UpdateContribTest extends UpdateTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'update',
    'update_test',
    'update_manager_project_security_status',
    'update_manager_project_security_status_test',
    'update_manager_project_security_status_test_aaa',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $admin_user = $this->drupalCreateUser(['administer site configuration']);
    $this->drupalLogin($admin_user);
  }

  /**
   * Tests that security coverage info is added to Update Manager report.
   */
  public function testUpdateContribBasic() {
    $system_info = [
      '#all' => [
        'version' => '8.0.0',
      ],
      'update_manager_project_security_status_test_aaa' => [
        'project' => 'update_manager_project_security_status_test_aaa',
        'version' => '1.0.4',
        'hidden' => FALSE,
      ],
    ];
    $this->config('update_test.settings')->set('system_info', $system_info)->save();
    $this->refreshUpdateStatus(
      [
        'drupal' => '0.0',
        'update_manager_project_security_status_test_aaa' => '1_0',
      ]
    );

    $this->assertSession()->responseContains('<h3>Modules</h3>');
    $this->assertSession()->pageTextContains('Covered by Drupal\'s security advisory policy');
  }

}
