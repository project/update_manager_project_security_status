<?php

namespace Drupal\update_manager_project_security_status_test\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   *
   * Override the update_test route to use our controller instead.
   */
  protected function alterRoutes(RouteCollection $collection) {
    if ($route = $collection->get('update_test.update_test')) {
      $route->setDefault('_controller', '\Drupal\update_manager_project_security_status_test\Controller\UpdateTestController::updateTest');
    }
  }

}
